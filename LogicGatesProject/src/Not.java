import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Not extends Gate
{

Not(ArrayList<Gate> gate)
{
	name = "not";
	hasGateA = true;
	hasGateB = false;
}

Not(Gate previousA, ArrayList<Gate> gate)
{
	if(previousA.getAssertionValue() == 0)
	{
		assertionValue = 1;
	}
	else
	{
		assertionValue = 0;
	}
	
	depth = calculateDepth(previousA);
	previousGateA = previousA;
	name = "not";
	hasGateA = true;
	hasGateB = false;
}

int calculateDepth(Gate previous)
{
	return previous.getDepth() + 1;
}

@Override
JLabel execute()
{
	JLabel notGate = null;
	if (assertionValue == 0)
	{/*
		System.out.println("\nDeasserted Not Gate\n");
		System.out.println("Depth is equal to " + depth);
		System.out.println("\n Height is equal to " + this.getHeight());
		*/
		notGate = new JLabel(new ImageIcon("./Pictures/NotDeasserted.png"));
	}
	else
	{/*
		System.out.println("\nAsserted Not Gate\n");
		System.out.println("Depth is equal to " + depth);
		System.out.println("\n Height is equal to " + this.getHeight());
		*/
		notGate = new JLabel(new ImageIcon("./Pictures/NotAsserted.png")); 
	}
	return notGate;
}
}
