import java.util.ArrayList;

public class GateHeight 
{

void calculateGateHeight(ArrayList<Gate>gateList)
{
	for(int i = 0; i < gateList.size(); i++)
	{
		int maxHeight = -1;
		for(int j = 0; j < gateList.size(); j++)
		{
		if(gateList.get(i).getDepth() == gateList.get(j).getDepth())
			{
			//find max height
			//print max height
				if(maxHeight < gateList.get(j).getHeight())
				{
					maxHeight = gateList.get(j).getHeight();
				}
			}
		}
		//System.out.println("\nSetting Gate Height");
		gateList.get(i).setGateHeight(maxHeight + 1);
	}
}
}
