import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class FalseGate extends Gate
{
	
	FalseGate()
	{
		assertionValue = 0;
		name = "false";
	}
	@Override
	JLabel execute()
	{
		JLabel falseGate = new JLabel(new ImageIcon("./Pictures/NANDDeasserted.png"));
		System.out.println("False Gate Always Deasserted");
		return falseGate;
	}
	
}
