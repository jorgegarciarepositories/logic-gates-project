import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Output extends Gate
{
	
int calculateDepth(ArrayList<Gate> gate)
{
	int largest = 0;
	for(int i = 0; i < gate.size(); i++)
	{
		if(!"output".equals(gate.get(i).getGateName()))
		{
		if(gate.get(i).getDepth() > largest)
		{
			largest = gate.get(i).getDepth();
		}
		}
	}
	return largest += 1;
}

Output(ArrayList<Gate> gate)
{
	name = "output";
	hasGateA = true;
	hasGateB = false;
}

Output(Gate previous, ArrayList<Gate> gate)
{
	if(previous.getAssertionValue() == 0)
	{
		assertionValue = 0;
	}
	else
	{
		assertionValue = 1;
	}
	
	depth = calculateDepth(gate);
	previousGateA = previous;
	name = "output";
	hasGateA = true;
	hasGateB = false;
}

@Override
JLabel execute()
{
	JLabel out = null;
	if(assertionValue == 0)
	{/*
		System.out.println("\nDeasserted Output\n");
		System.out.println("Depth is equal to " + depth);
		System.out.println("\n Height is equal to " + height);
		*/
		out = new JLabel(new ImageIcon("./Pictures/OutputDeasserted.png"));
	}
	else
	{/*
		System.out.println("\nAsserted Output\n");
		System.out.println("Depth is equal to: " + depth);
		System.out.println("\n Height is equal to " + height);
		*/
		out = new JLabel(new ImageIcon("./Pictures/OutputAsserted.png"));
	    
	}
	//System.out.println("GATE COMING INTO OUTPUT" + this.getPreviousGateA().getGateName());
	return out;
}

}
