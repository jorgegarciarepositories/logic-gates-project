import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Input extends Gate
{

Input(ArrayList<Gate> gate)
{
	name = "input";
	depth = 0;
	hasGateA = false;
	hasGateB = false;
}

Input(int isAsserted, ArrayList<Gate> gate)
{
	assertionValue = isAsserted;
	name = "input";
	depth = 0;
	hasGateA = false;
	hasGateB = false;
}

@Override
JLabel execute()
{
	JLabel input = null;
	if (assertionValue == 0)
	{
		System.out.println("\nInput Deasserted\n");
		System.out.println("Depth is equal to " + depth);
		System.out.println("\n Height is equal to " + height);
		input = new JLabel(new ImageIcon("./Pictures/InputDeasserted.png"));
	}
	else
	{
		System.out.println("\nInput Asserted\n");
		System.out.println("Depth is equal to " + depth);
		System.out.println("\n Height is equal to " + height);
		input = new JLabel(new ImageIcon("./Pictures/InputAsserted.png"));
	}
	return input;
	}
	@Override
	void activateTheInput()
	{
		if(assertionValue == 0)
		{
			assertionValue = 1;
		}
		else if (assertionValue == 1)
		{
			assertionValue = 0;
		}
		System.out.println("Input assertion values: "+assertionValue);
	}

}
