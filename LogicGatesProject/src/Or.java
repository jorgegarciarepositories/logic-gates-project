import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Or extends Gate
{

Or(ArrayList<Gate> gate)
{
	name = "or";
	hasGateA = true;
	hasGateB = true;
}

Or(Gate previousA, Gate previousB, ArrayList<Gate> gate)
{
	if(previousA.getAssertionValue() == previousB.getAssertionValue() && (previousA.getAssertionValue() == 0))
	{
		assertionValue = 0;
	}
	else
	{
		assertionValue = 1;
	}
	
	depth = calculateDepth(previousA.getDepth(), previousB.getDepth());
	previousGateA = previousA;
	previousGateB = previousB;
	name = "or";
	hasGateA = true;
	hasGateB = true;
}

@Override
JLabel execute()
{
	JLabel orGate = null;
	if (assertionValue == 0)
	{/*
		System.out.println("\nDeasserted OR gate\n");
		System.out.println("Depth is equal to " + depth);
		System.out.println("\n Height is equal to " + this.getHeight());
		*/
		orGate = new JLabel(new ImageIcon("./Pictures/ORDeasserted.png"));
	}
	else
	{/*
		System.out.println("\nAsserted OR gate\n");
		System.out.println("Depth is equal to " + depth);
		System.out.println("\n Height is equal to " + height);
		*/
		orGate = new JLabel(new ImageIcon("./Pictures/ORAsserted.png"));
	    
	}
	return orGate;
}
}
