import javax.swing.JFileChooser;

public class openButton {
	
	String path;
	
	public openButton() 
	{	
		JFileChooser fileChooser =  new JFileChooser();
		fileChooser.showOpenDialog(null);
		path = fileChooser.getSelectedFile().getAbsolutePath();
	}

	public String getPath()
	{
		return path;
	}
}


