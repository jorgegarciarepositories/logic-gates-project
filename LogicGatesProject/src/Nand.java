import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Nand extends Gate
{
Nand(ArrayList<Gate> gate)
{
	name = "nand";
	hasGateA = true;
	hasGateB = true;
}

Nand(Gate prevA, Gate prevB, ArrayList<Gate> gate)
{
	if(prevA.getAssertionValue() == prevB.getAssertionValue() && (prevB.getAssertionValue() ==1))
	{
		assertionValue = 0;
	}
	else
	{
		assertionValue = 1;
	}
	depth = calculateDepth(prevA.getDepth(), prevB.getDepth());
	previousGateA = prevA;
	previousGateB = prevB;
	name = "nand";
	hasGateA = true;
	hasGateB = true;
}
@Override
JLabel execute()
{
	JLabel nand = null;
	if (assertionValue == 0)
	{/*
		System.out.println("\nDeasserted NAND gate\n");
		System.out.println("Depth is equal to " + depth);
		System.out.println("\n Height is equal to " + height);
		*/
		nand = new JLabel(new ImageIcon("./Pictures/NANDDeasserted.png"));
	}
	else
	{/*
		System.out.println("\nAsserted NAND gate\n");
		System.out.println("Depth is equal to " + depth);
		System.out.println("\n Height is equal to " + height);
		*/
		nand = new JLabel(new ImageIcon("./Pictures/NANDAsserted.png"));
	}
	return nand;
}

}
