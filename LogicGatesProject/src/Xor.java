import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Xor extends Gate
{
Xor(ArrayList<Gate> gate)
{
	name = "xor";
	hasGateA = true;
	hasGateB = true;
}

Xor(Gate prevGateA, Gate prevGateB, ArrayList<Gate> gate)
{
	if(prevGateA.getAssertionValue() == prevGateB.getAssertionValue())
	{
		assertionValue = 0;
	}
	else
	{
		assertionValue = 1;
	}
	depth = calculateDepth(prevGateA.getDepth(), prevGateB.getDepth());
	previousGateA = prevGateA;
	previousGateB = prevGateB;
	name = "xor";
	hasGateA = true;
	hasGateB = true;
}

@Override
JLabel execute()
{
	JLabel xOr = null;
	if (assertionValue == 0)
	{/*
		System.out.println("\nDEAsserted Xor gate\n");
		System.out.println("Depth is equal to " + depth);
		System.out.println("\n Height is equal to " + height);
		*/
		xOr = new JLabel(new ImageIcon("./Pictures/XORDeasserted.png"));
	}
	else
	{/*
		System.out.println("\nAsserted Xor gate\n");
		System.out.println("Depth is equal to " + depth);
		System.out.println("\n Height is equal to " + height);
		*/
		xOr = new JLabel(new ImageIcon("./Pictures/XORAsserted.png"));
	    
	}
	return xOr;
}

}
