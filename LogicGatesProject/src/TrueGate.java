


import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class TrueGate extends Gate
{
	
	TrueGate()
	{
		assertionValue = 1;
		name = "true";
		hasGateA = false;
		hasGateB = false;
	}
	
	@Override
	JLabel execute()
	{
		JLabel trueGate = new JLabel(new ImageIcon("./Pictures/connector.png"));
		System.out.println("True Gate Always Asserted");
		return trueGate;
	}
	
}
