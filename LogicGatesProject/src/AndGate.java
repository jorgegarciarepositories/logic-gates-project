import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class AndGate extends Gate 
{
AndGate(ArrayList<Gate> gate)
{
	name = "and";
	hasGateA = true;
	hasGateB = true;
}

AndGate(Gate prevA, Gate prevB, ArrayList<Gate> gate)
{
	if(prevA.getAssertionValue() == prevB.getAssertionValue() && (prevB.getAssertionValue() != 0))
	{
		assertionValue = 1;
	}
	else
	{
		assertionValue = 0;
	}
	
	name = "and";
	depth = calculateDepth(prevA.getDepth(), prevB.getDepth());
	previousGateA = prevA;
	previousGateB = prevB;
	hasGateA = true;
	hasGateB = true;
	
}

@Override
JLabel execute()
{
	JLabel andGate = null;
	if (assertionValue == 0)
	{
		System.out.println("\nDeasserted AND gate\n");
		System.out.println("Depth is equal to" + depth);
		System.out.println("\n Height is equal to " + height);
		andGate = new JLabel(new ImageIcon("./Pictures/AndDeasserted.png"));
	}
	else
	{
		System.out.println("\nAsserted AND gate\n");
		System.out.println("Depth is equal to" + depth);
		System.out.println("\n Height is equal to " + height);
		andGate = new JLabel(new ImageIcon("./Pictures/AndAsserted.png"));
	}
	return andGate;
}

}
